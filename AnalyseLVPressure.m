function [AP_average,t] = AnalyseLVPressure(LVP_raw, debug)

TOL = 1e-4;


%% Select trigger time points.
time = LVP_raw(:,2);
LVP = LVP_raw(:,4);
trigger = abs(LVP_raw(:,11)-max(LVP_raw(:,11)));
figure(4)
[ax] = plotyy(time, LVP, time, trigger);
%plot(LVP_raw(:,2), LVP_raw(:,14), 'r');
title('Left ventricular trace with image trigger')
xlabel('Time (s)');
ylabel(ax(1), 'LV Pressure (kPa)', 'FontSize', 12);
ylabel(ax(2), 'Trigger Signal', 'FontSize',12);
%ylabel('Pressure (kPa)', 'Trigger signal');
%legend(['LV Pressure (kPa)', 'Trigger Signal'])

disp('Selecting raw trigger points')
if debug
    figure(5)
    plot(time, LVP);
    title('All trigger points marked', 'FontSize', 12);
    xlabel('Time (s)', 'FontSize', 12);
    ylabel('Pressure (kPa)', 'FontSize', 12);
end
hold on
n = 1;
for i = 1:length(trigger)
    if abs(trigger(i)) > TOL
        trigger_indices_raw(n) = i;
        if debug
            plot(time(i), LVP(i), 'r.');
        end
        n = n + 1;
    end
end


% Refine raw trigger points to pick out landmark points.
figure(6)
plot(time, LVP);
hold on;
title('Image trigger time points shown in green', 'FontSize', 12);
xlabel('Time (s)', 'FontSize', 12);
ylabel('LV Pressure (kPa)', 'FontSize', 12);
disp('Selecting final trigger points')
trigger_type = 'First';
counter = 1;
j = 1;
while j < length(trigger_indices_raw)
    % Group together trigger points which are close together temporally.
    k = 0;
    
    while (time(trigger_indices_raw(j+k))-time(trigger_indices_raw(j)))<0.05
        trigger_group(k+1) = trigger_indices_raw(j+k);
        k = k + 1;
        if j+k > length(trigger_indices_raw)
            break;
        end
    end
    switch (trigger_type)
        case 'First'
            trigger_points(counter) = trigger_group(1);
            plot(time(trigger_points(counter)), LVP(trigger_points(counter)), 'ko', 'MarkerSize', 4, 'MarkerFace', 'g');
            counter = counter + 1;
        case 'Last'
            trigger_points(counter) = max(trigger_group);
            plot(time(trigger_points(counter)), LVP(trigger_points(counter)), 'ko','MarkerSize', 4, 'MarkerFace', 'g');
            counter = counter + 1;
    end
    j = j + k;
end

%% Extract averaged trace using trigger points. 
[AP_average, t] = ExtractAverageTraces(LVP, time, trigger_points);
%R_R = dt*(size(AP_average)-1); % Calculate cycle duration
hold on
subplot(3,1,1),plot(t, AP_average, 'r+');

xlabel('Time (s)', 'FontSize', 12);
ylabel('LV Pressure (kPa)', 'FontSize', 12);
title('Averaged LV Pressure before temporal scaling', 'FontSize', 12);
grid on;

%% Find first and second derivatives
[dPdt, d2Pdt2] = Find1st2ndDerivatives(AP_average,t);

%% Plot end IVC (min)
% Plot end IVC
EndIVC_index = 2;
subplot(3,1,1), plot(t(EndIVC_index), AP_average(EndIVC_index), 'ko', 'MarkerSize',8, 'MarkerFace','y');
grid on;
subplot(3,1,2), plot(t(EndIVC_index), dPdt(EndIVC_index), 'ko', 'MarkerSize', 8, 'MarkerFace', 'y');
grid on;
subplot(3,1,3), plot(t(EndIVC_index), d2Pdt2(EndIVC_index), 'ko', 'MarkerSize', 8, 'MarkerFace', 'y');
grid on;
return
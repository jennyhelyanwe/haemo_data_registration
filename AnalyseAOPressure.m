function [AP_average, t, ES_index] = AnalyseAOPressure(AOP_raw, debug)

%% Extract data from AOP_raw
time = AOP_raw(:,2);
AOP = AOP_raw(:,4);

%% Extract local minima ponts for the aortic trace
SP = findStationaryPoints(AOP, 'FindMin', 'AOP', debug); % Find all local minima.
% Visualisation of local minima points available for selection. 
figure
plot(time, AOP);
title('Raw aortic pressure trace', 'FontSize', 12);
xlabel('Time (s)', 'FontSize', 12);
ylabel('Pressure (kPa)', 'FontSize', 12);
hold on
for i= 1:length(SP)
    plot(time(SP(i)), AOP(SP(i)),'ko', 'MarkerSize', 4, 'MarkerFace', 'g');
end

%% Generated averaged AOP trace
[AP_average, t] = ExtractAverageTraces(AOP, time, SP); % Select minima to average cycles. 
hold on
subplot(3,1,1),plot(t, AP_average, 'r+');
title('Averaged Aortic pressure trace', 'FontSize', 12);
xlabel('Time (s)');
ylabel('Pressure (kPa)');

%% Find 1st and 2nd Derivatives
dt = t(2) - t(1);
[dPdt, d2Pdt2] = Find1st2ndDerivatives(AP_average, t);
%% Extract end IVC (min) and ES (point of inflection)  
% Avoid false ES in first 0.2 seconds 
offset_avoid = 0.2;
d2Pdt2_partial = d2Pdt2(floor(offset_avoid/dt):(end-floor(0.1/dt)));
ES_index = find(d2Pdt2_partial==max(d2Pdt2_partial))+floor(offset_avoid/dt);
subplot(3,1,1), plot(t(ES_index), AP_average(ES_index), 'ko', 'MarkerSize',8, 'MarkerFace','g');
subplot(3,1,2), plot(t(ES_index), dPdt(ES_index), 'ko', 'MarkerSize', 8, 'MarkerFace', 'g');
subplot(3,1,3), plot(t(ES_index), d2Pdt2(ES_index), 'ko', 'MarkerSize', 8, 'MarkerFace', 'g');
% Plot end IVC
EndIVC_index = 2;
subplot(3,1,1), plot(t(EndIVC_index), AP_average(EndIVC_index), 'ko', 'MarkerSize',8, 'MarkerFace','y');
grid on;
subplot(3,1,2), plot(t(EndIVC_index), dPdt(EndIVC_index), 'ko', 'MarkerSize', 8, 'MarkerFace', 'y');
grid on;
subplot(3,1,3), plot(t(EndIVC_index), d2Pdt2(EndIVC_index), 'ko', 'MarkerSize', 8, 'MarkerFace', 'y');
grid on;
return


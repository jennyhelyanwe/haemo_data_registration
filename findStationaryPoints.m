function SP_final = findStationaryPoints(Pressure, option, option2, debug)
%% This function gives the array indices of the local maxima or minima of a
% pressure trace.
% The input options are:
% Option: Specifying whether the search is for the local maximum ('FindMax')
% or minimum ('FindMin')stationary points in a pressure trace.
% Option2: Specifying whether the pressure trace is for an aortic trace ('AOP')
% or a left ventricular pressure trace ('LVP').
% The difference being that for the LV pressure trace we also require the
% finding of the ED point using the peak pressure points.
% The output are the time points of the required stationary points (not
% their indices) as well as the pressure at those time points.
%
% 05/03/2014 JZW

temporal_spacing_Haemo=1; %% ms
%% Find local maxima points .
% Find gradient of pressure using forward difference.
dPdt = zeros(length(Pressure),1);
dP2d2t = dPdt;
for i = 1:(length(dPdt)-1)
    dPdt(i) = Pressure(i+1) - Pressure(i);
end
% Use backward differences for the last gradient point.
dPdt(end) = Pressure(end) - Pressure(end-1);

% Find second derivative
for i = 1:(length(dP2d2t) -1)
    dP2d2t(i) = dPdt(i+1) - dPdt(i);
end
dP2d2t(end) = dPdt(end) - dPdt(end-1);

if strcmp(option, 'FindMax')
    % Find peak points
    counter = 1;
    k = 1;
    for i = 1:(length(dPdt)-1)
        if (dPdt(i) == 0)||(sign(dPdt(i)) ~= sign(dPdt(i+1)))
            statpt(k) = i; % Save all stationary points.
            k=k+1;
            if dP2d2t(i) < 0
                maxpt(counter) = i; % the array indices of maximum points
                counter = counter + 1;
            end
        end
    end
    figure
    plot(Pressure);
    hold on
    for i = 1:length(maxpt)
        plot(maxpt(i),Pressure(maxpt(i)),'r.');
    end
    % Apply window
    t_window = 500; % ms
    indices_window = floor(t_window/temporal_spacing_Haemo);
    windowoffset = input('Please enter the windowoffset for finding peak points (<300)...\n');
    start_index = 1 + windowoffset;
    end_index = start_index + indices_window;
    numWindows = floor((length(Pressure)-windowoffset)/indices_window);
    
    for i = 1:numWindows+1
        % find all peak points within the window
        maxpts_current(:,1) = maxpt(find((maxpt > start_index)&(maxpt<end_index)));
        maxpts_current(:,2) = Pressure(maxpts_current(:,1));
        start_index = end_index;
        end_index = end_index + indices_window;
        
        % locate a single peak stationary point within each window.
        temp = find(maxpts_current(:,2) == max(maxpts_current(:,2)));
        peak(i,1) = maxpts_current(temp(1),1);
        peak(i,2) = Pressure(peak(i,1));
        
        plot(peak(i,1), peak(i,2), 'g.');
        
        
        % deallocate maxpts_current
        clear maxpts_current
    end
    % Take into account the duplication due to bad windowing
    k = 1;
    i = 1;
    while i < length(peak)
        if abs(peak(i,1)-peak(i + 1,1)) < 310
            temp = find(peak(:,2)== max(peak(i,2), peak(i+1,2)));
            SP_final(k,1) = peak(temp(1),1);
            SP_final(k,2) = max(peak(i,2), peak(i+1, 2));
            k = k+1;
            i = i+2;
        else
            SP_final(k,1) = peak(i,1);
            SP_final(k,2) = peak(i,2);
            k = k+1;
            i = i+1;
        end
    end
    SP_final(k,:) = peak(end,:);
    if debug
        % Show final results
        figure
        plot(Pressure);
        hold on
        for i = 1:length(SP_final)
            plot(SP_final(i,1), SP_final(i,2),'r.');
        end
    end
elseif strcmp(option,'FindMin')
    %% Find the local minima points.
    counter = 1;
    k = 1;
    for i = 1:(length(dPdt)-1)
        if (dPdt(i) == 0)||(sign(dPdt(i))~= sign(dPdt(i+1)))
            statpt(k) = i;
            k = k + 1;
            if dP2d2t(i) > 0
                minpt(counter) = i;
                counter = counter + 1;
            end
        end
    end
    
    figure
    plot(Pressure);
    hold on
    for i = 1:length(minpt)
        plot(minpt(i), Pressure(minpt(i)), 'r.');
    end
    
    % Apply window
    t_window = 500;
    indices_window = floor(t_window/temporal_spacing_Haemo);
    windowoffset = input('Please enter the window offset for finding local minima points...\n');
    start_index = 1 + windowoffset;
    end_index = start_index + indices_window;
    numWindows = floor((length(Pressure)-windowoffset)/indices_window);
    
    for i = 1:numWindows + 1
        % Find all minimum points within the window
        minpts_current(:,1) = minpt(find((minpt>start_index)&(minpt<end_index)));
        minpts_current(:,2) = Pressure(minpts_current(:,1));
        start_index = end_index;
        end_index = end_index + indices_window;
        
        % locate a single minimum stationary point within each window.
        temp = find(minpts_current(:,2) == min(minpts_current(:,2)));
        trough(i,1) = minpts_current(temp(1),1);
        trough(i,2) = Pressure(trough(i,1));
        
        plot(trough(i,1), trough(i,2), 'g.');
        
        % Deallocate minpts_current
        clear minpts_current
    end
    % Take into account the duplication due to bad windowing.
    k = 1;
    i = 1;
    while i < length(trough)
        if abs(trough(i,1)-trough(i+1, 1)) < 300
            temp = find(trough(:,2) == min(trough(i,2), trough(i+1,2)));
            SP_final(k,1) = trough(temp(1),1);
            SP_final(k,2) = min(trough(i,2), trough(i+1, 2));
            k = k + 1;
            i = i + 2;
        else
            SP_final(k,1) = trough(i,1);
            SP_final(k,2) = trough(i,2);
            k = k + 1;
            i = i + 1;
        end
    end
    SP_final(k,:) = trough(end, :);
    if debug
        % Show final results
        figure
        plot(Pressure)
        hold on
        for i = 1:length(SP_final)
            plot(SP_final(i,1), SP_final(i,2), 'r.');
        end
    end
end


%% Find ED points also for LVPressure analysis
if strcmp(option2, 'LVP') % for LVPressure analysis we require ED points also.
    for i = 2:length(SP_final)
        % find the index of stationary point for the current peak point
        statpt_i = find(statpt == SP_final(i,1));
        previous_statpt_i = find(statpt == SP_final(i-1,1));
        % Find the first stat pnt which is less than a third of the height
        % of the peak point
        while statpt_i > previous_statpt_i;
            if Pressure(statpt(statpt_i)) < 0.3*Pressure(SP_final(i,1));
                ED_point(i-1,1) = statpt(statpt_i);
                ED_point(i-1,2) = Pressure(ED_point(i-1,1));
                if debug
                    plot(ED_point(i-1,1), ED_point(i-1,2),'k.');
                end
                break;
            else
                statpt_i = statpt_i - 1;
            end
        end
    end
    temp = SP_final;
    k = 1;
    for i = 1:length(SP_final)-1
        SP_final(k,:) = temp(i,:);
        SP_final(k+1,:) = ED_point(i,:);
        k= k + 2;
    end
    SP_final(k,:) = temp(end,:);
end
return
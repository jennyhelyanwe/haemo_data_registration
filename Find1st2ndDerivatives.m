function [dPdt, d2Pdt2] = Find1st2ndDerivatives(AP_average, t)
% Find first derivative using backward differences
dt = t(2) - t(1);  % Temporal spacing of haemodynamic measurement. 
dPdt = zeros(1, (size(AP_average,1)));
for i = 2:size(dPdt, 2)
    dPdt(i) = (AP_average(i) - AP_average(i-1))/dt; % Calculate derivative
end
% Apply smoother to first derivative
dPdt_sm = zeros(1, (size(dPdt, 2)-1));
for i = 1:size(dPdt, 2)
    if i>2 & i<(size(dPdt,2)-2)
        dPdt_sm(i) = (2*dPdt(i-2) - dPdt(i-1) + dPdt(i+1) + 2*dPdt(i+2))/(dt*0.0001*10);
    else
        dPdt_sm(i) = dPdt(i);
    end
end
dPdt = dPdt_sm;

subplot(3,1,2), plot(t, dPdt, 'b*');
title('Aortic Pressure 1st Derivative','FontSize', 12);
xlabel('Time (s)', 'FontSize', 12);
ylabel('dP/dt (kPa/s)', 'FontSize', 12);
hold on
% Find second derivative using backward differences. 
d2Pdt2 = zeros(1,size(AP_average, 1));
for i = 1:(size(d2Pdt2,2)-1)
    d2Pdt2(i+1) = (dPdt(i+1)- dPdt(i))/dt;
end
% Apply smoother to second derivative. 
for i = 1:size(d2Pdt2,2)
    if i>2&i<(size(d2Pdt2,2)-2);
        d2Pdt2_sm(i) = (2*d2Pdt2(i-2)-d2Pdt2(i-1)+d2Pdt2(i+1)+2*d2Pdt2(i+2))/(dt*0.01*10);
    else
        d2Pdt2_sm(i) = dPdt(i);
    end
end
d2Pdt2 = d2Pdt2_sm;
subplot(3,1,3), plot(t, d2Pdt2, 'k*-');
title('Aortic Pressure 2nd Derivative','FontSize', 12);
xlabel('Time (s)', 'FontSize', 12);
ylabel('dP/dt (kPa/s)', 'FontSize', 12);
hold on
return
function [] = PressureRegistration_Triggers()
%% This script file deals with the human data from Renee's myocardial
% infarction studies. The file reads in raw aortic and left ventricular
% pressure traces from .mat files (which had been generated from raw excel
% files) and provides a user-customisable method of selecting the pressure
% cycles for averaging.
% The aortic and left ventricular pressure traces are then aligned to give
% a more accurate interpolation of pressure values at each MRI frame. These
% pressure values are essential for parameter estimation processes.

%% Housekeeping
clc
clear all
close all
debug = false;
% LVP_raw = xlsread('ET7_PostMI.xlsx', 'LVP');
% save LVP_raw_ET7_PostMI LVP_raw
% AOP_raw = xlsread('ET7_PostMI.xlsx', 'Aortic');
% save AOP_raw_ET7_PostMI AOP_raw


%% Load in data
load LVP_raw_ET7_PostMI
load AOP_raw_ET7_PostMI
%% Visualise triggers and ECG.
if debug
    figure(1)
    for i=3:19
        subplot(9,2,(i-2)),plot(AOP_raw(:,2),AOP_raw(:,i));
        title([num2str(i)]);
    end
    figure(2)
    plotyy(AOP_raw(:,2), AOP_raw(:,4), AOP_raw(:,2), AOP_raw(:,14));
    
    figure(3)
    for i = 3:19
        subplot(9,2,(i-2)), plot(LVP_raw(:,2), LVP_raw(:,i));
        title([num2str(i)]);
    end
end

%% Analyse LVP trace
[LVP_Average,LVP_t] = AnalyseLVPressure(LVP_raw, debug);

%% Analyse AOP trace

[AOP_Average, AOP_t, ES_index] = AnalyseAOPressure(AOP_raw, debug);

%% Plot LVP and AOP together
% For sake of validation of previous framework, we now overlay the aortic
% trace on top of this average trace using the minimum point as the
% alignment marker. 
scrsz = get(0, 'ScreenSize');
figure('Position', [scrsz(4)/3 scrsz(2)/3 scrsz(3)/3 scrsz(3)])
subplot(3,1,1), plot(LVP_t, LVP_Average, 'b*');
hold on
subplot(3,1,1), plot(AOP_t, AOP_Average, 'r*');

return
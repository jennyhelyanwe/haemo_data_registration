function [AP_average, t] = ExtractAverageTraces(Pressure, time, SP)
%% This function extracts pressure cycles from raw traces according to user
% specified identifying special points (SP). 

num_cycles = input('Enter number of pressure cycles to averaged: ');
selectedSP = zeros(num_cycles, 1);
for i = 1:num_cycles
    selectedSP(i) = input('Enter the local minimum number for selected cycle: ');
    plot(time(SP(selectedSP(i))), Pressure(SP(selectedSP(i))), 'k.');
end
scrsz = get(0, 'ScreenSize');
figure('Position', [scrsz(4)/3 scrsz(2)/3 scrsz(3)/3 scrsz(3)])
hold on
dt = time(2) - time(1);
AP = [];
for i = 1:num_cycles - 1
    AP_cycle = Pressure(SP(selectedSP(i)):SP(selectedSP(i)+1));
    num_sample_cycle = size(AP_cycle, 1);
    
    if i > 1
        if num_sample_cycle>num_sample
            AP_cycle((num_sample(1)+1):num_sample_cycle) = [];
        elseif num_sample_cycle<num_sample
            AP_cycle((num_sample_cycle+1):num_sample) = mean(reshape(AP(((num_sample_cycle+1):num_sample(1)), :), 1, []));
        end
    else
        num_sample = num_sample_cycle;
    end
    t = linspace(0, dt*num_sample, num_sample);
    subplot(3,1,1),plot(t, AP_cycle, 'b*');
    AP = [AP, AP_cycle];
end

%% Calculate the average pressure
AP_average = mean(AP, 2);
return
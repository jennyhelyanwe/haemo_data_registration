function Pressure_Registration
%% This function registers the pressure and MRI image frames for the
% cardiac cycle. It has been written based on previous code which is
% primarily written by Vicky Wang. 
%
% The input text file is called file_io.txt. It should contain the
% following information (note, each piece of information should be on a new
% line, and there should be 8 lines in total):
% 1. study_name: the name of the xlsx file which contains the raw pressure
% traces.
% 2. tot_frame: the total number of MRI frames.
% 3. ED_frame: the MRI frame number for ED. 
% 4. endIVC_frame
% 5. ES_frame
% 6. endIVR_frame
% 7. DS_frame
% 8. R_R_interval: the average R and R interval of the cardiac cycle. 
%
% NB: The xlsx file must have two sheets, named 'Aortic' and 'LVP', in
% which the 2nd column is the time in seconds (starting from 0), and the
% 4th column is the pressure value in kPa. 
%
% Modification by: Zhinuo Jenny Wang 8th May 2014

%% Housekeeping
clear all;
clc;
close all;

fprintf('\n********** Start to analyse aortic pressure **************\n'); 
temporal_spacing_pressure = 1;   % ms. Time spacing between each haemodynamic pressure measurement. 

%% File I/O
fid = fopen('file_io.txt','r');
study_name = fgetl(fid);
tot_frame = str2num(fgetl(fid));
ED_frame = str2num(fgetl(fid));
endIVC_frame = str2num(fgetl(fid));
ES_frame = str2num(fgetl(fid));
endIVR_frame = str2num(fgetl(fid));
DS_frame = str2num(fgetl(fid));
R_R_interval = str2num(fgetl(fid)); % ms
fclose(fid);

%% Obtain raw pressure traces
Aortic_read = xlsread([study_name '.xlsx'], 'Aortic');
Aortic_raw(:,1) = Aortic_read(:,2);
Aortic_raw(:,2) = Aortic_read(:,4);

LVP_read = xlsread([study_name '.xlsx'], 'LVP');
LVP_raw(:,1) = LVP_read(:,2);
LVP_raw(:,2) = LVP_read(:,4);

% Visualise raw traces. 
plot(Aortic_raw(:,1), Aortic_raw(:,2))
title('Aortic raw pressure trace')
ylabel('Pressure (kPa)')
xlabel('Time (s)')
figure
plot(LVP_raw(:,1), LVP_raw(:,2))
title('Left Ventricular raw pressure trace')
ylabel('Pressure (kPa)')
xlabel('Time (s)')

%% Analyse aortic raw pressure trace. 
fprintf('\n************ Start to analyse aortic pressure *************\n');

AOP = ExtractAorticPressure(Aortic_raw, study_name);
% Print peak to peak interval. 
fprintf('===== The peak to peak duration for aortic pressure is %f =====\n', AOP.AOP_Average_HaemoTT(1,end));
fprintf('\n***********************************************************\n');

%% Analyse left ventricular raw pressure trace.
fprintf('\n************ Start to analyse LV pressure *************\n');

LVP = ExtractLVPressure(LVP_raw, study_name);
% Print peak to peak interval.
fprintf('===== The peak to peak duration for LV pressure is %f =====\n', LVP.LVP_Average_HaemoTT(1,end));
fprintf('\n***********************************************************\n');

%% Visualise aortic and LV pressure traces on one plot. 
scrsz = get(0, 'ScreenSize');
figure('Position',[scrsz(4)/3 scrsz(2)/3 scrsz(3)/2.5 scrsz(3)])
subplot(3,1,1),plot(AOP.AOP_Average_HaemoTT(1,:),AOP.AOP_Average_HaemoTT(2,:),'r*');
hold on;
subplot(3,1,1),plot(LVP.LVP_Average_HaemoTT(1,:),LVP.LVP_Average_HaemoTT(2,:),'b*');
grid on;
subplot(3,1,1),plot(AOP.AOP_Average_HaemoTT(1,AOP.EndStages(1,1)),AOP.AOP_Average_HaemoTT(2,AOP.EndStages(1,1)),'ko','MarkerSize',8,'MarkerFace','y');
hold on;
subplot(3,1,1),plot(AOP.AOP_Average_HaemoTT(1,AOP.EndStages(1,2)),AOP.AOP_Average_HaemoTT(2,AOP.EndStages(1,2)),'ko','MarkerSize',8,'MarkerFace','g');
hold on;
xlabel('Time (s)', 'FontSize', 12);
ylabel('Pressure (kPa)', 'FontSize', 12);
title(['LV and Aortic pressure for study ', study_name], 'FontSize', 12);
legend('Aortic Pressure', 'LV Pressure', 'EndIVC', 'ES');

%% Select ES, end IVC and end IVR time points for the LVP based on the AOP.
% Select end IVC

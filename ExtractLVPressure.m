function LVPressure=ExtractLVPressure(Pressure,study_name)

%% This function is designed to extract LVP at DS, ED and ES from the haemo
%% data

%%%%%%%%%%%%%%%%%%%%%%%%%% Read in Pressure Data from xls %%%%%%%%%%%%%%%%%
%% Read in the excel data
%[Pressure] = xlsread(study_name);
debug = true;
%% Locate all stationary points identified by Jane
%Stationary_Points=find(isnan(Pressure(:,1)));
Stationary_Points = findStationaryPoints(Pressure(:,2),'FindMax', 'LVP', debug);

%% Ask user to enter the starting point and number of cycle
SP_Start=input('Please enter the starting stationary point....\n');
No_cycle=input('Please enter the number of cycles....\n');

LVP=[];
temporal_spacing_Haemo=1; %% ms
no_sample_points=[];
LVP_Peak_ED=[];

scrsz = get(0,'ScreenSize');
figure('Position',[scrsz(4)/3 scrsz(2)/3 scrsz(3)/3 scrsz(3)])
%% Isolate pressure from multiple cycles
for i=1:No_cycle
    if size(Pressure,2)==3
        LVP_Cycle=Pressure(Stationary_Points(SP_Start):(Stationary_Points(SP_Start+2)-1),3);
    else
        LVP_Cycle=Pressure(Stationary_Points(SP_Start):(Stationary_Points(SP_Start+2)-1),2);
    end
    % Calculate the duration between peak and ED
    LVP_Peak_ED_Cycle=Stationary_Points(SP_Start+1,1)-Stationary_Points(SP_Start,1)+1;
    LVP_Peak_ED=[LVP_Peak_ED;LVP_Peak_ED_Cycle];
    no_sample_points_cycle=size(LVP_Cycle,1);
    time=linspace(0,temporal_spacing_Haemo*(no_sample_points_cycle-1),no_sample_points_cycle);
    subplot(3,1,1),plot(time,LVP_Cycle,'b*');
    hold on;
    if i>1
        if no_sample_points_cycle>no_sample_points(1)
            LVP_Cycle((no_sample_points(1)+1):no_sample_points_cycle)=[];
            no_sample_points_cycle=size(LVP_Cycle,1);
        elseif no_sample_points_cycle<no_sample_points(1)
            LVP_Cycle((no_sample_points_cycle+1):no_sample_points(1))=mean(LVP(((no_sample_points_cycle+1):no_sample_points(1)),:),2);
        end
    end
    LVP=[LVP,LVP_Cycle];
    SP_Start=SP_Start+2;
    no_sample_points=[no_sample_points;no_sample_points_cycle];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%% Calculate Average LV Pressure %%%%%%%%%%%%%%%%
%% Calculate the average pressure
LVP_Average=mean(LVP,2);
R_R_Haemo=temporal_spacing_Haemo*(size(LVP_Average,1)-1);
TT_Haemo=linspace(0,R_R_Haemo,size(LVP_Average,1));
subplot(3,1,1),plot(TT_Haemo,LVP_Average,'r+');
axis([0 ceil(max(TT_Haemo*1.1)) 0 ceil(max(LVP_Average)*1.5)]);
hold on;
xlabel('Time (ms)','FontSize',12);
ylabel('LV Pressure (mmHg)','FontSize',12);
title('HaemoData Before Temporal Scaling','FontSize',12);
grid on;
%% Calculate the R-R interval for the Haemo data
LVP_Average_TT(1,:)=TT_Haemo;
LVP_Average_TT(2,:)=LVP_Average;
LVPressure.LVP_Average_HaemoTT=LVP_Average_TT;
%% Calculate the average duration between peak and ED
LVP_Peak_ED_Average=ceil(mean(LVP_Peak_ED));

%%%%%%%%%%%%%%%%%%%%%%%%%%%% Calculate 1st derivatives %%%%%%%%%%%%%%%%%%%%
LVP_deriv_1=zeros(1,(size(LVP_Average,1)-1));
for i=1:size(LVP_deriv_1,2)
    if i==1 | i==size(LVP_deriv_1,2)
        LVP_deriv_1(i)=(LVP_Average(i+1)-LVP_Average(i))/(temporal_spacing_Haemo*0.001);
    else
        %% forward difference
        LVP_deriv_1(i)=(LVP_Average(i+1)-LVP_Average(i))/(temporal_spacing_Haemo*0.001);
        %% 2-point central difference
        %LVP_deriv_1(i)=(LVP_Average(i+1)-LVP_Average(i-1))/(0.004*2);
        %% 3-point central difference
        %LVP_deriv_1(i)=(LVP_Average(i+1)-2*LVP_Average(i)+LVP_Average(i-1))/(0.004^2);
        %LVP_deriv_1(i)=(2*LVP_Average(i-2)-LVP_Average(i-1)+LVP_Average(i+1)+2*LVP_Average(i+2))/(0.004*10);
    end
    %LVP_deriv_1(i)=(LVP_Average(i+1)-LVP_Average(i))/0.004;
end
TT_Haemo_deriv_1=linspace(TT_Haemo(2),R_R_Haemo,size(LVP_deriv_1,2));
subplot(3,1,2),plot(TT_Haemo_deriv_1,LVP_deriv_1,'b*');
axis([0 ceil(max(TT_Haemo*1.1)) ceil(min(LVP_deriv_1)*1.1) ceil(max(LVP_deriv_1)*1.1)]);
grid on;
title(['LV Pressure 1st Derivative Study ',study_name],'FontSize',12);
xlabel('Time (ms)','FontSize',12);
ylabel('dp/dt','FontSize',12);
hold on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%% Calculate 2nd derivatives %%%%%%%%%%%%%%%%%%%%
LVP_deriv_2=zeros(1,(size(LVP_deriv_1,2)-1));
for i=1:size(LVP_deriv_2,2)
    if i==1 | i==size(LVP_deriv_2,2)
        LVP_deriv_2(i)=(LVP_deriv_1(i+1)-LVP_deriv_1(i))/0.004; %% forward difference
    else
        %% forward difference
        LVP_deriv_2(i)=(LVP_deriv_1(i+1)-LVP_deriv_1(i))/0.004;
        %% 2-point central difference
        %LVP_deriv_2(i)=(LVP_deriv_1(i+1)-LVP_deriv_1(i-1))/(0.004*2);
        %% 3-point central difference
        %LVP_deriv_2(i)=(LVP_deriv_1(i+1)-2*LVP_deriv_1(i)+LVP_deriv_1(i-1))/(0.004^2); 
    end
end

%% Apply a smoother to the 2nd derivatives
LVP_deriv_2_sm=zeros(1,(size(LVP_deriv_1,2)-1));
for i=1:size(LVP_deriv_2,2)
    if i>2 & i<(size(LVP_deriv_2,2)-2)
        LVP_deriv_2_sm(i)=(2*LVP_deriv_2(i-2)-LVP_deriv_2(i-1)+LVP_deriv_2(i+1)+2*LVP_deriv_2(i+2))/(0.004*10);
    else
        LVP_deriv_2_sm(i)=LVP_deriv_2(i);
    end
end
LVP_deriv_2=LVP_deriv_2_sm;
TT_Haemo_deriv_2=linspace(TT_Haemo(3),R_R_Haemo,size(LVP_deriv_2,2));
subplot(3,1,3),plot(TT_Haemo_deriv_2,LVP_deriv_2,'k*-');
axis([0 ceil(max(TT_Haemo*1.1)) ceil(min(LVP_deriv_2)*1.1) ceil(max(LVP_deriv_2)*1.1)]);
title(['LV Pressure 2nd Derivative Study ',study_name],'FontSize',12);
xlabel('Time (ms)','FontSize',12);
ylabel('dp^2/dt^2','FontSize',12);
grid on;
hold on;

%%%%%%%%%%%%%%%%%%%% Find the minimum LVP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
LVP_DS_index=find(LVP_Average==min(LVP_Average));
LVP_DS=LVP_Average(LVP_DS_index);
LVP_ED_index=LVP_Peak_ED_Average;
LVP_ED=LVP_Average(LVP_ED_index);
%{
LVP_Peak_EndIVR=LVP_Average(1:50);
LVP_EndIVR_ED_diff=LVP_Peak_EndIVR-LVP_ED;
LVP_EndIVR_index=find(LVP_EndIVR_ED_diff==min(LVP_EndIVR_ED_diff));
LVP_EndIVR=LVP_Peak_EndIVR(LVP_EndIVR_index);
%}
%{ 
minimum dp/dt doesn't appear to correlate with End IVR
%%%%%%%%%%%%%%%%%%%% Find the minimum dp/dt %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
LVP_EndIVR_index=find(LVP_deriv_1==min(LVP_deriv_1));
LVP_EndIVR=LVP_Average(LVP_EndIVR_index);
LVPressure.EndStages(1,:)=[LVP_DS_index,LVP_ED_index,0,0,LVP_EndIVR_index];
LVPressure.EndStages(2,:)=[LVP_DS,LVP_ED,0,0,LVP_EndIVR];
%}

%%%%%%%%%%%%%%%%%%%% Find the maximum d2p/dt2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
LVP_EndIVR_index=find(LVP_deriv_2==max(LVP_deriv_2(1:floor(size(LVP_deriv_2,2)/2))));
LVP_EndIVR=LVP_Average(LVP_EndIVR_index(1)+2);
%%%%% So far the if statement below is only applied to STF-08
if (LVP_EndIVR-LVP_DS(1))>=20 
    %% If the pressure drop between EndIVR and DS is greater than 20mmHg, then
    %% there is like to be a 2nd local maximum closer to DS, which would be 
    %% the true LVP @ end IVR
    LVP_deriv_2_cropped=LVP_deriv_2((LVP_EndIVR_index+1):floor(size(LVP_deriv_2,2)/2));
    [pks,locs]=findpeaks(LVP_deriv_2_cropped,'minpeakdistance',5);
    LVP_EndIVR_index=LVP_EndIVR_index+locs(1);
    LVP_EndIVR=LVP_Average(LVP_EndIVR_index(1)+2);
end
%}

%% End-stage orders: DS, ED, EndIVC, ES, EndIVR
LVPressure.EndStages(1,:)=[LVP_DS_index(1),LVP_ED_index(1),0,0,(LVP_EndIVR_index(1)+2)];
LVPressure.EndStages(2,:)=[LVP_DS(1),LVP_ED(1),0,0,LVP_EndIVR(1)];

subplot(3,1,1),plot(LVP_Average_TT(1,LVPressure.EndStages(1,1)),LVP_Average_TT(2,LVPressure.EndStages(1,1)),'ko','MarkerSize',8,'MarkerFace','m');
hold on;
subplot(3,1,1),plot(LVP_Average_TT(1,LVPressure.EndStages(1,2)),LVP_Average_TT(2,LVPressure.EndStages(1,2)),'ko','MarkerSize',8,'MarkerFace','c');
hold on;
subplot(3,1,1),plot(LVP_Average_TT(1,LVPressure.EndStages(1,5)),LVP_Average_TT(2,LVPressure.EndStages(1,5)),'ko','MarkerSize',8,'MarkerFace','k');
hold on;


subplot(3,1,2),plot(TT_Haemo_deriv_1(LVPressure.EndStages(1,5)-1),LVP_deriv_1(LVPressure.EndStages(1,5)-1),'ko','MarkerSize',8,'MarkerFace','k');

subplot(3,1,3),plot(TT_Haemo_deriv_2(LVPressure.EndStages(1,5)-2),LVP_deriv_2(LVPressure.EndStages(1,5)-2),'ko','MarkerSize',8,'MarkerFace','k');


return